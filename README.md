Report

Approach
	1. 3 gram, 2 gram
	I wanted to try a new approach for hw3. On hearing that n grams was an option and reading into it, I decided to adopt this method to 		homophone correction. 
	The idea is to detect 3 grams and 2grams associated with homophones from the training data. Each time a homophone is found in the 		training file, the 3 grams and 2 grams associated with it are stored. The n grams are stored with a keyword indicating their position. 		This ensures that position context information of the n gram is saved. If the ngram was already present its count is increased.
	On being given test data, all the homophones present in the file are detected. The probability of the homphone and its alternative are 		calculated by summing up the values of the associated 3 grams. If the alternative word has a higher probability then it is written in 		the output in place of the input homophone. If 3 grams aren't found for either word, the same approach is adopted with 2 grams. If 2 		grams aren't found the word is assumed to be correct and is left unchanged.
		
	Example:
		She had everything to lose and nothing to gain.		
		Relevant 3grams- "everything to lose", "to lose and", "lose and nothing"

		This is saved as
		3gram["prev-everything to"]["lose"]+=1
		3gram["curr-to and"]["lose"]+=1
		3gram["next-and nothing"]["lose"]+=1

	NOTE: The decision to limit the ngrams to 3 grams was taken based on the amount of input data I could collect. Initially with smaller 		amounts of data, 2 grams was seen to do better than 3 gram. However as the amount of data increased 3 grams was seen to do better. 		Unfortunately there wasn't enough data to implement 4 grams 

	2. 1 gram
	The one gram probability for each of the homophones was also calculated. However on observing that using one gram reduced FScores, I 		decided against using them. If a homophone doesn't have any associated 3 gram or 2 gram it is assumed to be correct.

	3. Microsoft n gram API
	The Microsoft n gram API returns the probability for any phrase provided. The idea was to use this api for words which weren't well 		represented in the training data. Words like "they're" fell in this category and had a very low fScore.  

		http://weblm.research.microsoft.com/info/index.html
		http://blogs.msdn.com/b/webngram/archive/2010/10/04/language-modelling-101.aspx
	
	However using this API required a user token. I could not obtain this within the deadline. As a result the idea to use this API was 		abandoned.

	4. Corpus used
	See Sources

Code
There are 2 python files required to run the code.
	1. train.py
	This is used to parse through text files and generate 3 grams and 2 grams which have the keyword in it. 
	As an argument it accepts a list of folder locations. It then parses through all the text files in these folders and generates 3 grams 		and 2 grams. The output is a set of n grams and associated count ie the number of times they have appeared in the input text files. It 		also outputs the list of n grams found so far.

		time python3 train.py outputFolderLocation inputFilesFolderLocation

	2. correct.py
	This program accepts the erraneous file, makes all the homophones replacements necessary. It them writes out the data ensuring that 		format of words other than homophones are maintained.
	Any corrected homophone has been outputed in the same upper or lower case style as the origin file.
	Considering word+punctuation type words for replacement improved the average fScore by around 1 percent.
	
		time python3 correct.py ngramFolderLocation erraneousFile correctedFile

	3. fScoreCalculator.py
	This programs accepts 2 files - a file known to be correct and a file that has been corrected by the program. It then counts the 	number of homophones and correctly classified homophones to generate the fscores for each file. It then publishes this data.
	Developing this code early on has been extremely useful in understanding how much of an impact a particular data source has in 		correcting a specific homophone.
	
		time python3 fScoreCalculator.py developmentFile correctedDevFile
	
	At last count the average fScores for the dev data is : 82%
	
	4. Other files:
		testDataStats.ods
	This contains the Fscore for each homophone and the sources which were used in training. This was useful in seeing how the fScore 	  altered on including additional sources.

		trainOutput folder
	Contains count of 3grams and 2 grams found. Also contains a mapper to get idex associated with each 3gram.

Softwares Used
No external softwares were used. I did attempt to use Microsoft N gram API.

Sources:
The following corpus was used to train the 3 gram file:

	1. Complexity of Word Collocation Networks: A Preliminary Structural Analysis
		Lahiri, Shibamouli	
	Used to obtain a cleaned dataset of around 3000 files from Project Gutenberg

	2.Open American National Corpus
	  MASC
	
	3. http://corpora.informatik.uni-leipzig.de/download.html





