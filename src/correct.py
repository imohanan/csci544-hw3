#
#	Name: Indu Mohanan
#	email: imohanan@usc.edu
#

import sys
import json
import os
import re

class Corrector:	

	def __init__(self):
		args=sys.argv
		self.outputlines=[]
		jsonData={}

		with open(args[1]+"Onegram.json","r",encoding="utf-8") as jsonFile:
			self.Onegram=json.load(jsonFile)
		with open(args[1]+"Twogram.json","r",encoding="utf-8") as jsonFile:
			self.Twogram=json.load(jsonFile)
		with open(args[1]+"Threegram.json","r",encoding="utf-8") as jsonFile:
			self.Threegram=json.load(jsonFile)
		with open(args[1]+"OnegramMapper.json","r",encoding="utf-8") as jsonFile:
			self.OnegramMapper=json.load(jsonFile)
		with open(args[1]+"TwogramMapper.json","r",encoding="utf-8") as jsonFile:
			self.TwogramMapper=json.load(jsonFile)
		with open(args[1]+"ThreegramMapper.json","r",encoding="utf-8") as jsonFile:
			self.ThreegramMapper=json.load(jsonFile)
		self.wordsToBeFound=['to','too','loose','lose',"they're",'their',"you're",'your',"it's",'its']
		self.punctuation=['_',':',';','(',')','"','-','*',',','.',',','?','!']
		self.bicount=0
		self.tricount=0
		self.totalwordsFound=0
	
	def setWordPairs(self):
		self.wordPairs={}
		self.wordPairs["it's"]="its"
		self.wordPairs["its"]="it's"
		self.wordPairs["you're"]="your"	
		self.wordPairs["your"]="you're"
		self.wordPairs["they're"]="their"
		self.wordPairs["their"]="they're"
		self.wordPairs["loose"]="lose"
		self.wordPairs["lose"]="loose"
		self.wordPairs["to"]="too"
		self.wordPairs["too"]="to"
	
	def readdata(self, fileLocation):
		with open(fileLocation, "r") as File:
				self.lines=File.readlines()

	def writedata(self, fileLocation):
		outputFile=open(fileLocation, "w") 
		outputFile.close()
		for line in self.outputlines:
			with open(fileLocation, "a") as File:
				File.write(line)

	def correct(self):
		for line in self.lines:
			for keyword in self.wordsToBeFound:
				keywordTitle=keyword.capitalize()
				countWord=0
				newline=False
				# todo: look at formatting before searching for word and getting indices, this might give more words
				strings1=re.findall(keyword,line)
				strings2=re.findall(keywordTitle,line)
				length=len(strings1)+len(strings2)
				if length!=0: 
					countWord+=length
					indices,words=self.getIndices(line,keyword)
					for index in indices:
						#1. get trigrams
						newline=self.correctByThreegrams(index,words,keyword)
						if newline is not False and newline is not True: line=newline
						if newline is not False: continue
						#2. else get bigrams
						newline=self.correctByTwograms(index,words,keyword)
						if newline is not False and newline is not True: line=newline
						#3. else get unigrams
						#if line==False:
						#newline=self.correctByUnigrams(index, line)
			self.outputlines.append(line)
		print(self.bicount)
		print(self.tricount)
		print(self.totalwordsFound)
		
	def correctByThreegrams(self,index,words,keyword):
		keywordAlternative=self.wordPairs[keyword]
		keywordIndex=self.OnegramMapper[keyword]
		keywordAlternativeIndex=self.OnegramMapper[keywordAlternative]
		listOfIndices=[]
		listOfWords=self.getWords(index,words)
		for wordSet in listOfWords:
			if wordSet not in self.ThreegramMapper: 
				continue
			listOfIndices.append(self.ThreegramMapper[wordSet])
		if len(listOfIndices)==0: return False
		self.totalwordsFound+=len(listOfIndices)
		keywordValue=0
		keywordAlternativeValue=0
		for wordindex in listOfIndices:
			keywordValue+=self.Threegram[wordindex][keywordIndex]
			keywordAlternativeValue+=self.Threegram[wordindex][keywordAlternativeIndex]
		if keywordValue>keywordAlternativeValue: return True
		self.tricount+=1
		words[index] = self.getWordStyled(words[index],keywordAlternative)
		line=" ".join(words)
		return line	

	def correctByTwograms(self,index,words,keyword):
		keywordAlternative=self.wordPairs[keyword]
		keywordIndex=self.OnegramMapper[keyword]
		keywordAlternativeIndex=self.OnegramMapper[keywordAlternative]
		if words[index][0] in self.punctuation: prevWord='prev '+words[index][0]
		else: prevWord='prev '+self.getformattedWord(words[index-1])[-1] if index>0 else 'BOS'
		if words[index][-1] in self.punctuation:nextWord='next '+words[index][-1]
		else:nextWord='next '+self.getformattedWord(words[index+1])[0] if index<(len(words)-2) else 'EOS'
		if prevWord not in self.TwogramMapper and nextWord not in self.TwogramMapper: return False
		self.totalwordsFound+=1
		keywordValue=0
		keywordAlternativeValue=0
		for word in prevWord,nextWord:
			if word in self.TwogramMapper:
				wordIndex=self.TwogramMapper[word]
				keywordValue+=self.Twogram[wordIndex][keywordIndex]
				keywordAlternativeValue+=self.Twogram[wordIndex][keywordAlternativeIndex]
		if keywordValue>keywordAlternativeValue: return True
		self.bicount+=1
		words[index] = self.getWordStyled(words[index],keywordAlternative)
		line=" ".join(words)
		return line	

	def getWords(self,index,words):
		listOfWords=[]
		word1=	words[index-2] if index>1 else 'BOS'
		word2=  words[index-1] if index>0 else 'BOS'
		word3=  words[index+1] if index<(len(words)-2) else 'EOS'
		word4=	 words[index+2] if index<(len(words)-3) else 'EOS'
		if(words[index][0] in self.punctuation): prevWords=self.getformattedWord(word1+" "+word2+" "+words[index][0])
		else: prevWords=self.getformattedWord(word1+" "+word2)
		if(words[index][-1] in self.punctuation): nextWords=self.getformattedWord(words[index][-1]+" "+word3+" "+word4)
		else: nextWords=self.getformattedWord(word3+" "+word4)
		listOfWords.append("prev "+prevWords[-2]+" "+prevWords[-1])
		listOfWords.append("curr "+prevWords[-1]+" "+nextWords[0])
		listOfWords.append("next "+nextWords[0]+" "+nextWords[1])
		return listOfWords

	def getformattedWord(self, word):
		word=word.replace('_',' _ ')
		word=word.replace(':',' :')
		word=word.replace(';',' ;')
		word=word.replace('(',' (')
		word=word.replace(')',' )')
		word=word.replace('"',' "')
		word=word.replace('-',' - ')
		word=word.replace('*',' *')
		word=word.replace('\t',' ')	
		word=word.replace(',',' ,')
		word=word.replace('.',' .')
		word=word.replace(',',' ,')
		word=word.replace('?',' ?')
		word=word.replace('!',' !')
		word=word.replace('  ',' ')
		return word.split()
		
	def getIndices(self,line,keyword):
		keywordTitle=keyword.capitalize()
		words=line.split(' ')
		count=0
		indices=[]
		while count< len(words):
			if words[count]==keyword or words[count]==keywordTitle:
				indices.append(count)
			if words[count][-1] in self.punctuation and (words[count][0:-2] ==keyword or words[count][0:-2]==keywordTitle):
				indices.append(count)
			if words[count][0] in self.punctuation and (words[count][1:] ==keyword or words[count][1:]==keywordTitle):
				print(words[count])
				indices.append(count)
			count+=1
		return indices,words
		
	def correctByUnigrams(self, index, line ):
		words=line.split()
		# toDo: review if this needs to be done! Maybe assuming correct is a better option
		keyword=words[index].lower()
		keywordAlternative=self.wordPairs[keyword]
		keywordIndex=self.OnegramMapper[keyword]
		keywordAlternativeIndex=self.OnegramMapper[keywordAlternative]
		if self.Onegram[keywordIndex] > self.Onegram[keywordAlternativeIndex]:	return
		#maintain word style
		words[index] = self.getWordStyled(words[index],keywordAlternative)
		line=" ".join(words)
		return line	
			
	def getWordStyled(self, keyword,keywordAlternative ):
		if(keyword[-1] in self.punctuation and keyword[0].islower()): return keywordAlternative+keyword[-1]
		elif(keyword[-1] in self.punctuation and keyword[0].isupper()): return keywordAlternative.capitalize()+keyword[-1]
		elif(keyword[0] in self.punctuation and keyword[1].islower()): return keyword[0]+keywordAlternative
		elif(keyword[0] in self.punctuation and keyword[1].isupper()): return keyword[0]+keywordAlternative.capitalize()
		elif keyword[0].islower():	return 	keywordAlternative
		elif keyword[0].isupper():  return keywordAlternative.capitalize()
		return keywordAlternative	

	def formatText(self,text):
		text=text.replace('\n',' ')
		text=text.replace('\t',' ')	
		text=text.replace(',',' ,')
		text=text.replace('.',' .')
		text=text.replace(',',' ,')
		text=text.replace('?',' ?')
		text=text.replace('  ',' ')
		return text



if __name__=='__main__':
	args=sys.argv
	corrector=Corrector()
	corrector.setWordPairs()
	corrector.readdata(args[2])
	corrector.correct()
	corrector.writedata(args[3])
	
	

