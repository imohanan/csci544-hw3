#
#	Name: Indu Mohanan
#	email: imohanan@usc.edu
#

import sys
import json
import os
import re

class Calculator:	

	def __init__(self):
		self.wordList=['to','too','loose','lose',"they're",'their',"you're",'your',"it's",'its']
		self.wordTotalCorrect={}
		self.wordTotalClassified={}
		self.wordCorrectlyClassified={}
		for word in self.wordList:
			self.wordTotalCorrect[word]=0
			self.wordTotalClassified[word]=0
			self.wordCorrectlyClassified[word]=0

	def calculate(self):
		args=sys.argv
		with open(args[1], "r") as correctFile:
			correctLines=correctFile.readlines()
		with open(args[2], "r") as outputFile:
			outputLines=outputFile.readlines()
		# 1. get total correct
		for keyword in self.wordList:
			keywordTitle=keyword.title()
			for line in correctLines:
				line=self.formatText(line)
				words=line.split()			
				cIndices1 = [i for i, x in enumerate(words) if x == keyword]
				cIndices2 = [i for i, x in enumerate(words) if x == keywordTitle]
				self.wordTotalCorrect[keyword]+=len(cIndices1)+len(cIndices2)
		# 2. get total classified
		for keyword in self.wordList:
			keywordTitle=keyword.title()
			for line in outputLines:
				line=self.formatText(line)
				words=line.split()			
				oIndices1 = [i for i, x in enumerate(words) if x == keyword]
				oIndices2 = [i for i, x in enumerate(words) if x == keywordTitle]
				self.wordTotalClassified[keyword]+=len(oIndices1)+len(oIndices2)
		# 3. get total correctly classified
		for keyword in self.wordList:
			keywordTitle=keyword.title()
			linecount=0
			while linecount<len(correctLines):
				correctline= self.formatText(correctLines[linecount])
				outputline= self.formatText(outputLines[linecount])
				linecount+=1
				correctwords=correctline.split()
				outputwords=outputline.split()
				cIndices1 = [i for i, x in enumerate(correctwords) if x == keyword]
				cIndices2 = [i for i, x in enumerate(correctwords) if x == keywordTitle]	 
				cIndices=cIndices1+cIndices2
				oIndices1 = [i for i, x in enumerate(outputwords) if x == keyword]
				oIndices2 = [i for i, x in enumerate(outputwords) if x == keywordTitle]	 
				oIndices=oIndices1+oIndices2
				count=0
				for index in cIndices:
					if index in oIndices: 	
						self.wordCorrectlyClassified[keyword]+=1
						count+=1
		
		for keyword in self.wordList:
			precision=(self.wordCorrectlyClassified[keyword])/(self.wordTotalClassified[keyword])
			recall=self.wordCorrectlyClassified[keyword]/self.wordTotalCorrect[keyword]
			fscore=(2*precision*recall)/(precision+recall)		
			print(keyword)		
			#print("Correctly Classified "+str(self.wordCorrectlyClassified[keyword]))
			#print("TotalClassified "+str(self.wordTotalClassified[keyword])	)
			#print("TotalCorrect "+str(self.wordTotalCorrect[keyword]))		
			print("\t"+str(precision))
			print("\t"+str(recall)	)
			print("\t"+str(fscore))	


	def formatText(self,text):
		text=text.replace('_',' _ ')
		text=text.replace(':',' :')
		text=text.replace(';',' ;')
		text=text.replace('(',' (')
		text=text.replace(')',' )')
		text=text.replace('"',' "')
		text=text.replace('-',' - ')
		text=text.replace('*',' *')
		text=text.replace('\t',' ')	
		text=text.replace(',',' ,')
		text=text.replace('.',' .')
		text=text.replace(',',' ,')
		text=text.replace('?',' ?')
		text=text.replace('!',' !')
		text=text.replace('  ',' ')
		return text

if __name__=='__main__':
	args=sys.argv
	# args[1] is correct file; args[2] is test file
	calculator=Calculator()
	calculator.calculate()
	
	

