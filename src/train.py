#
#	Name: Indu Mohanan
#	email: imohanan@usc.edu
#

import sys
import json
import os
import re


class Trainer:	

	def __init__(self):
		args=sys.argv
		
		with open(args[1]+"ThreegramMapper.json","r",encoding="utf-8") as jsonFile:
			string = jsonFile.readlines()
			print(len(string))
			print(hello)
			threegramMapper=json.load(jsonFile)
		self.ThreegramMapper={} if len(threegramMapper) ==0 else threegramMapper
		with open(args[1]+"Onegram.json","r",encoding="utf-8") as jsonFile:
			onegramData=json.load(jsonFile)
		self.Onegram=[0 for x in range(10)] if len(onegramData) ==0 else onegramData
		with open(args[1]+"Twogram.json","r",encoding="utf-8") as jsonFile:
			twogramData=json.load(jsonFile)
		self.Twogram=[[0 for x in range(10)] for x in range(1000000)] if len(onegramData) ==0 else twogramData
		with open(args[1]+"Threegram.json","r",encoding="utf-8") as jsonFile:
			threegramData=json.load(jsonFile)
		self.Threegram=[[0 for x in range(10)] for x in range(1000000)] if len(threegramData) ==0 else threegramData
		with open(args[1]+"OnegramMapper.json","r",encoding="utf-8") as jsonFile:
			onegramMapper=json.load(jsonFile)
		self.OnegramMapper={} if len(onegramMapper) ==0 else onegramMapper
		with open(args[1]+"TwogramMapper.json","r",encoding="utf-8") as jsonFile:
			twogramMapper=json.load(jsonFile)
		self.TwogramMapper={} if len(twogramMapper) ==0 else twogramMapper
		
		self.TwogramMapperCount=len(self.TwogramMapper)
		self.ThreegramMapperCount=len(self.ThreegramMapper)
		self.filescovered=[]
	
	def trainSource1(self, folderLocation):
		wordsToBeFound=['to','too','loose','lose',"they're",'their',"you're",'your',"it's",'its']
		for filename in os.listdir(folderLocation):
			if ".txt" not in filename:
				continue
			with open(folderLocation+"/"+filename, "r", errors="ignore") as File:
				lines=File.readlines()
			for line in lines:
				text=self.formatText(line)
				for keyword in wordsToBeFound:
					indices,words=self.findStrings(text,keyword)
					if indices is None:	continue
					self.MapOnegram(keyword,len(indices))
					self.MapTwogram(keyword,indices,words)
					self.MapThreegram(keyword,indices,words)
		return None

	def trainSource2(self, folderLocation):
		wordsToBeFound=['to','too','loose','lose',"they're",'their',"you're",'your',"it's",'its']
		for filename in os.listdir(folderLocation):
			with open(folderLocation+"/"+filename, "r", errors="ignore") as File:
				lines=File.readlines()
			for line in lines:
				text=self.formatText(line)
				for keyword in wordsToBeFound:
					indices,words=self.findStrings(text,keyword)
					if indices is None:	continue
					self.MapOnegram(keyword,len(indices))
					self.MapTwogram(keyword,indices,words)
					self.MapThreegram(keyword,indices,words)
		return None

	def findStrings(self,text,keyword):
		keywordTitle=keyword.capitalize()
		words=text.split()
		if (keyword not in words) and (keywordTitle not in words): return None,None
		indices1 = [i for i, x in enumerate(words) if x == keyword]
		indices2 = [i for i, x in enumerate(words) if x == keywordTitle]
		indices=indices1+indices2
		return indices,words
		

	def formatText(self,text):
		text=text.replace('_',' _ ')
		text=text.replace(':',' :')
		text=text.replace(';',' ;')
		text=text.replace('(',' (')
		text=text.replace(')',' )')
		text=text.replace('"',' "')
		text=text.replace('-',' - ')
		text=text.replace('*',' *')
		text = re.sub("\d+", "", text)
		text=text.replace('\t',' ')	
		text=text.replace(',',' ,')
		text=text.replace('.',' .')
		text=text.replace(',',' ,')
		text=text.replace('?',' ?')
		text=text.replace('!',' !')
		text=text.replace('  ',' ')
		return text

	def MapThreegram(self,keyword,indices,words):
		keywordIndex= self.OnegramMapper[keyword]
		listOfWords=self.getWords(indices,words)
		for wordSet in listOfWords:
			if wordSet not in self.ThreegramMapper:
				self.ThreegramMapper[wordSet]=self.ThreegramMapperCount
				self.ThreegramMapperCount+=1
			wordIndex= self.ThreegramMapper[wordSet]
			if wordIndex>(len(self.Threegram)-1):
				self.Threegram.append([0 for x in range(10)])
			#print(wordIndex)
			#print(len(self.Threegram))
			# toDo:append to list here instead of predefining list size
			self.Threegram[wordIndex][keywordIndex]+=1

	def getWords(self,indices,words):
		listOfWords=[]
		for index in indices:
			word1=	words[index-2] if index>1 else 'BOS'
			word2=  words[index-1] if index>0 else 'BOS'
			word3=  words[index+1] if index<(len(words)-1) else 'EOS'
			word4=  words[index+2] if index<(len(words)-2) else 'EOS'
			listOfWords.append("prev "+word1+" "+word2)
			listOfWords.append("curr "+word2+" "+word3)
			listOfWords.append("next "+word3+" "+word4)
		return listOfWords

	def MapTwogram(self,keyword,indices,words):
		for index in indices:
			prevWord='prev '+words[index-1] if index>0 else 'BOS'
			nextWord='next '+words[index+1] if index<(len(words)-1) else 'EOS'
		if prevWord not in self.TwogramMapper:
			self.TwogramMapper[prevWord]=self.TwogramMapperCount
			self.TwogramMapperCount+=1
		if nextWord not in self.TwogramMapper:
			self.TwogramMapper[nextWord]=self.TwogramMapperCount
			self.TwogramMapperCount+=1
		#2. increment weightage of index in 3gram Mapper
		wordIndex= self.OnegramMapper[keyword]
		prevIndex= self.TwogramMapper[prevWord]
		nextIndex= self.TwogramMapper[nextWord]
		self.Twogram[prevIndex][wordIndex]+=1
		self.Twogram[nextIndex][wordIndex]+=1

	def MapOnegram(self,word,count):
		#1. see if word1 is in Mapper dict. Get index of words
		if word not in self.OnegramMapper:
			self.OnegramMapper[word]=self.OnegramMapperCount
			self.OnegramMapperCount+=1
		#2. increment weightage of index in 3gram Mapper
		index= self.OnegramMapper[word]
		self.Onegram[index]+=count

	def saveData(self):
		jsonData={}
		jsonData['Onegram']=self.Onegram
		jsonData['Twogram']=self.Twogram
		jsonData['Threegram']=self.Threegram
		jsonData['OnegramMapper']=self.OnegramMapper
		jsonData['TwogramMapper']=self.TwogramMapper
		jsonData['ThreegramMapper']=self.ThreegramMapper
		with open(args[1]+"Onegram.json","w+",encoding="utf-8") as out_file:
			json.dump(jsonData['Onegram'],out_file, indent=4, ensure_ascii=False)
		with open(args[1]+"Twogram.json","w+",encoding="utf-8") as out_file:
			json.dump(jsonData['Twogram'],out_file, indent=4, ensure_ascii=False)
		with open(args[1]+"Threegram.json","w+",encoding="utf-8") as out_file:
			json.dump(jsonData['Threegram'],out_file, indent=4, ensure_ascii=False)
		with open(args[1]+"OnegramMapper.json","w+",encoding="utf-8") as out_file:
			json.dump(jsonData['OnegramMapper'],out_file, indent=4, ensure_ascii=False)
		with open(args[1]+"TwogramMapper.json","w+",encoding="utf-8") as out_file:
			json.dump(jsonData['TwogramMapper'],out_file, indent=4, ensure_ascii=False)
		with open(args[1]+"ThreegramMapper.json","w+",encoding="utf-8") as out_file:
			json.dump(jsonData['ThreegramMapper'],out_file, indent=4, ensure_ascii=False)

	def saveTodb():
		self._db = MongoClient().nlphw3
			for key,val in self.OnegramMapper:
				Onegram={}
				Onegram['key'] = key
				Onegram['value']=value
				self._db.OnegramMapper.save(Onegram)
		
if __name__=='__main__':
	#1. load already trained data
	trainer=Trainer()
	print(hi)
	args=sys.argv
	sourceCount=2
	while sourceCount<len(args):
		function=getattr(trainer,"trainSource1")
		if function: function(args[sourceCount])
		sourceCount+=1
		print("Trained "+args[sourceCount-1])
	trainer.saveData()
	
